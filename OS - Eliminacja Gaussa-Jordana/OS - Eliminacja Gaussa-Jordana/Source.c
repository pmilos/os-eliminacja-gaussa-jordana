/* Liczenie macierzy odwrotnej metodą Gaussa - Jordana */
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>



int main()
{
	system("mode 240,60");
	printf("            ****** LICZENIE MACIERZY ODWROTNEJ METODA GAUSSA - JORDANA ******\n\n");

	int i, j, k, n;

	float d;           // zmienna przechowująca wartości sumowania poszczególnych elementów
	float **a = NULL; // tablica przechowująca dane

	float **b = NULL; // tablica do wyświetlania etapów obliczeń w konsoli
	float **w = NULL; // macierz wynikowa (sprawdzenie)
	float **G = NULL; // tablica obliczeniowa przechowująca macierz wprowadzoną przez użytkownika
	float **H = NULL; // tablica obliczeniowa przechowująca wynik obliczeń (macierz odwrotna)


	printf("Podaj liczbe rownan: ");

	scanf_s("%i", &n);

	/*alokacja pamięci dla tablicy przechowującej dane*/
	a = (float**)calloc((n*2)+1, sizeof(float*));

	for (size_t i = 0; i != (n * 2) + 1; i++)
	{
		a[i] = (float*)calloc((n * 2) + 1, sizeof(float));

		if (a[i] == NULL)
		{
			printf("Nie udalo sie zarezerwowac pamieci.\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	/*alokacja pamięci dla tablicy 'b'*/
	b = (float**)calloc((n * 2) + 1, sizeof(float*));

	for (size_t i = 0; i != (n * 2) + 1; i++)
	{
		b[i] = (float*)calloc((n * 2) + 1, sizeof(float));

		if (b[i] == NULL)
		{
			printf("Nie udalo sie zarezerwowac pamieci.\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	/*alokacja pamięci dla tablicy 'w'*/
	w = (float**)calloc((n + 1), sizeof(float*));

	for (size_t i = 0; i != (n + 1); i++)
	{
		w[i] = (float*)calloc((n + 1), sizeof(float));

		if (w[i] == NULL)
		{
			printf("Nie udalo sie zarezerwowac pamieci.\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	/*alokacja pamięci dla tablicy 'G'*/
	G = (float**)calloc((n + 1), sizeof(float*));

	for (size_t i = 0; i != (n + 1); i++)
	{
		G[i] = (float*)calloc((n + 1), sizeof(float));

		if (G[i] == NULL)
		{
			printf("Nie udalo sie zarezerwowac pamieci.\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	/*alokacja pamięci dla tablicy 'H'*/
	H = (float**)calloc((n + 1), sizeof(float*));

	for (size_t i = 0; i != (n + 1); i++)
	{
		H[i] = (float*)calloc((n + 1), sizeof(float));

		if (H[i] == NULL)
		{
			printf("Nie udalo sie zarezerwowac pamieci.\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	printf("Wpisz wspolczynniki macierzy z kolumna wyrazow wolnych: \n");
	/*wczytywanie wprowadzonej macierzy do tablicy operacyjnej*/
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n; j++)
		{
			scanf_s("%f", &(a[i][j]));
			/*kopiowanie wprowadzonej macierzy do późniejszego sprawdzenia wyniku*/
			b[i][j] = a[i][j];
			G[i][j] = a[i][j];
		}
	}
	

	/*tworzenie macierzy jednostkowej rozmiaru wprowadzonej macierzy*/
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= 2 * n; j++)
		{
			if (j == (i + n))
			{
				a[i][j] = 1;
			}
		}
	}
	printf("\nWprowadzona macierz: \n");
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n * 2; j++)
			printf("%f  ", a[i][j]);
		puts("");
	}

	/* Szukanie pierwszego elementu macierzy do rozpoczecia algorytmu */
	for (i = n; i>1; i--)
	{
		if (a[i - 1][1]<a[i][1])
			for (j = 1; j <= n * 2; j++)
			{
				d = a[i][j];
				a[i][j] = a[i - 1][j];
				a[i - 1][j] = d;
			}
	}
	puts("");

	printf("Odwrocona macierz: \n");
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n * 2; j++)
			printf("%f  ", a[i][j]);
		puts("");
	}
	puts("");

	/* Redukcja macierzy do postaci diagonalnej */
	printf("Redukcja macierzy do postaci diagonalnej: \n");
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n * 2; j++)
			if (j != i)
			{
				d = a[j][i] / a[i][i];
				for (k = 1; k <= n * 2; k++)
					a[j][k] -= a[i][k] * d;
			}
	}


	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n; j++)
			printf(" %f  ", a[i][j]);
		puts("");
	}
	puts("");


	/* Redukcja macierzy do postaci jednostkowej */
	printf("Redukcja macierzy do postaci jednostkowej: \n");

	for (i = 1; i <= n; i++)
	{
		d = a[i][i];
		for (j = 1; j <= n * 2; j++)
			a[i][j] = a[i][j] / d; 
	}

	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n * 2; j++)
			printf("%f  ", a[i][j]);
		puts("");
	}

	puts("");

	printf( "Macierz wynikowa: \n"); // wynikiem jest macierz odwrotna

	for (i = 1; i <= n; i++)
	{
		for (j = n + 1; j <= n * 2; j++)
			printf(" %f ", a[i][j]);
		puts("");
	}
	puts("");

	for (i = 1; i <= n; i++)
	{
		for (j = n + 1; j <= n * 2; j++)
		{
			b[i][j] = a[i][j];
		}
	}

	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n ; j++)
		{
			H[i][j] = b[i][n + j];
		}
	}

	
	for (i = 1; i <= n; i++)
	{
		for (j = n + 1; j <= n * 2; j++)
		{
			a[i][j] = a[n - i][2 * n - j];
		}
	}

	printf("Sprawdzenie: \n"); // przedstawienie mnożenia macierzy

	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n * 2; j++)
			printf(" %f ", b[i][j]);
			puts("");
	}
	puts("");


	/*Obliczanie sprawdzenia wyniku*/
	for (j = 1; j < n + 1; j++)
	{
		for (i = 1; i < n + 1; i++)
		{
			for (k = 1; k < n + 1; k++)
			{
				w[i][j] += G[i][k] * H[k][j]; // wynik += macierz wprowadzona * macierz odwrotna
			}
		}
	}

	printf("Wynik: \n"); // prawidłowym wynikiem sprawdzenia jest macierz jednostkowa

	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n; j++)
			printf(" %f ", w[i][j]);
		puts("");
	}
	puts("");



	system("PAUSE");
	return EXIT_SUCCESS;
}